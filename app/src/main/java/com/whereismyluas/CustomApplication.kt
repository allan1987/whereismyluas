package com.whereismyluas

import android.app.Application
import com.whereismyluas.dagger.AppComponent
import com.whereismyluas.dagger.AppModule
import com.whereismyluas.dagger.DaggerAppComponent

class CustomApplication: Application() {
    lateinit var appComponent: AppComponent

    private fun initDagger(app: CustomApplication): AppComponent =
        DaggerAppComponent.builder()
            .appModule(AppModule(app))
            .build()

    override fun onCreate() {
        super.onCreate()
        appComponent = initDagger(this)
    }
}