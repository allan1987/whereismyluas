package com.whereismyluas.dagger

import com.whereismyluas.MainActivity
import com.whereismyluas.ui.main.MainFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, ViewModelModule::class, NetworkModule::class])
interface AppComponent {

    fun inject(target: MainActivity)
    fun inject(target: MainFragment)
}


