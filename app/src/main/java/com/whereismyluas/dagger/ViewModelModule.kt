package com.whereismyluas.dagger

import com.whereismyluas.ui.main.model.MainRepository
import com.whereismyluas.ui.main.MainViewModel
import com.whereismyluas.network.StopInfoApi
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ViewModelModule {
    @Provides
    @Singleton
    fun provideMainRepository(stopInfoApi: StopInfoApi): MainRepository = MainRepository(stopInfoApi)

    @Provides
    @Singleton
    fun provideMainViewModel(mainRepo: MainRepository): MainViewModel = MainViewModel(mainRepo)
}
