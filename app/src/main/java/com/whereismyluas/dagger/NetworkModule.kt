package com.whereismyluas.dagger

import com.whereismyluas.network.StopInfoApi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.simplexml.SimpleXmlConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
class NetworkModule {

    companion object {
        private const val NAME_BASE_URL = "https://luasforecasts.rpa.ie/xml/"
    }

    @Provides
    @Named(NAME_BASE_URL)
    fun provideBaseUrlString() = NAME_BASE_URL

    @Provides
    @Singleton
    fun provideHttpClient() = OkHttpClient()

    @Provides
    @Singleton
    fun provideRetrofitBuilder() = Retrofit.Builder()

    @Provides
    @Singleton
    fun provideRetrofit(builder: Retrofit.Builder, @Named(NAME_BASE_URL) baseUrl: String, okHttpClient: OkHttpClient) =
        builder.baseUrl(baseUrl)
            .client(okHttpClient)
            .addConverterFactory(SimpleXmlConverterFactory.create())
            .build()

    @Provides
    @Singleton
    fun provideStopInfoApi(retrofit: Retrofit) = retrofit.create(StopInfoApi::class.java)
}