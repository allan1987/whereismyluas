package com.whereismyluas

class Constants {

    companion object {
        const val FORECAST = "forecast"
        const val STOP_MAR = "mar"
        const val STOP_STI = "sti"
        const val INFO_MARLBOROUGH = "Trams from Marlborough LUAS stop"
        const val INFO_STILLORGAN = "Trams from Stillorgan LUAS stop"
        const val TOWARDS_INBOUND = "Towards Inbound:"
        const val TOWARDS_OUTBOUND = "Towards Outbound:"
        const val INBOUND = "Inbound"
        const val OUTBOUND = "Outbound"
    }
}