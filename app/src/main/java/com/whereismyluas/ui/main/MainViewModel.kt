package com.whereismyluas.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.whereismyluas.Constants
import com.whereismyluas.ui.main.model.MainRepository
import com.whereismyluas.ui.main.model.Tram
import kotlinx.coroutines.launch
import java.text.DateFormat
import java.util.*
import javax.inject.Inject

class MainViewModel @Inject constructor(private val mainRepository: MainRepository): ViewModel() {

    private val _tramList = MutableLiveData<List<Tram>>()
    val tramList: LiveData<List<Tram>> = _tramList

    private val _title = MutableLiveData<String>()
    val title: LiveData<String> = _title

    private val _lastUpdate = MutableLiveData<String>()
    val lastUpdate: LiveData<String> = _lastUpdate

    private val _direction = MutableLiveData<String>()
    val direction: LiveData<String> = _direction

    private val _errorMessage = MutableLiveData<Unit>()
    val errorMessage: LiveData<Unit> = _errorMessage

    private val _progress = MutableLiveData<Boolean>()
    val progress: LiveData<Boolean> = _progress

    fun refresh() {
        viewModelScope.launch {
            kotlin.runCatching {
                _progress.postValue(true)
                mainRepository.getStopInfo()
            }.onSuccess {
                with(it.stopInfo) {
                    _lastUpdate.postValue(DateFormat.getDateTimeInstance().format(Date()))

                    this.directionList.find { d ->
                        if (it.isInbound) d.name == Constants.INBOUND
                        else d.name == Constants.OUTBOUND
                    }?.run {
                        if (tramList.size == 1 && tramList[0].dueMins.isEmpty()) {
                            _title.postValue(message)
                            _direction.postValue(tramList[0].destination)
                            _tramList.postValue(emptyList())
                        } else {
                            if (it.isInbound) {
                                _title.postValue(Constants.INFO_STILLORGAN)
                                _direction.postValue(Constants.TOWARDS_INBOUND)
                            } else {
                                _title.postValue(Constants.INFO_MARLBOROUGH)
                                _direction.postValue(Constants.TOWARDS_OUTBOUND)
                            }
                            _tramList.postValue(tramList)
                        }
                    }
                    _progress.postValue(false)
                }
            }.onFailure {
                it.printStackTrace()
                _errorMessage.postValue(Unit)
                _progress.postValue(false)
            }
        }
    }
}