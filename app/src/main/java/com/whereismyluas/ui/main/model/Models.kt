package com.whereismyluas.ui.main.model

import android.text.TextUtils
import org.simpleframework.xml.*
import java.util.*

class StopInfoInbound(val stopInfo: StopInfo, val isInbound: Boolean)

@Root (name = "stopInfo", strict = false)
data class StopInfo constructor(
    @field:Element(name = "message")
    @param:Element(name = "message")
    val message: String,
    @field:ElementList(entry = "direction", inline = true, type = Direction::class)
    @param:ElementList(entry = "direction", inline = true, type = Direction::class)
    val directionList: List<Direction>
)

@Root(name = "direction", strict = false)
data class Direction constructor(
    @field:Attribute(name = "name")
    @param:Attribute(name = "name")
    val name: String,
    @field:ElementList(entry = "tram", inline = true, type = Tram::class)
    @param:ElementList(entry = "tram", inline = true, type = Tram::class)
    val tramList: List<Tram>
)

@Root(name = "tram", strict = false)
data class Tram constructor(
    @field:Attribute(name = "dueMins")
    @param:Attribute(name = "dueMins")
    val dueMins: String,
    @field:Attribute(name = "destination")
    @param:Attribute(name = "destination")
    val destination: String
) {
    override fun toString(): String {
        return if (TextUtils.isDigitsOnly(dueMins)) {
            if (dueMins == "1") "$destination: $dueMins min"
            else "$destination: $dueMins mins"
        } else {
            "$destination: $dueMins"
        }
    }
}