package com.whereismyluas.ui.main.model

import com.whereismyluas.Constants
import com.whereismyluas.network.StopInfoApi
import java.util.*

class MainRepository(private val stopInfoApi: StopInfoApi) {

    suspend fun getStopInfo(): StopInfoInbound {
        with(Calendar.getInstance()) {
            when (get(Calendar.HOUR_OF_DAY)) {
                in 0..11 -> Constants.STOP_MAR
                in 13..23 -> Constants.STOP_STI
                else -> if (get(Calendar.MINUTE) == 0) Constants.STOP_MAR
                else Constants.STOP_STI
            }.run {
                return StopInfoInbound(
                    stopInfoApi.getStopInfo(Constants.FORECAST, this@run, false),
                    this@run == Constants.STOP_STI
                )
            }
        }
    }
}