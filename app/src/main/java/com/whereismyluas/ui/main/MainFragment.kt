package com.whereismyluas.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.whereismyluas.CustomApplication
import com.whereismyluas.R
import com.whereismyluas.dagger.AppComponent
import com.whereismyluas.dagger.ViewModelModule
import com.whereismyluas.ui.main.model.Tram
import javax.inject.Inject

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var tvTitle: TextView
    private lateinit var tvLastUpdatedTime: TextView
    private lateinit var tvDirection: TextView
    private lateinit var rvTrams: RecyclerView
    private lateinit var pbProgress: ProgressBar
    private lateinit var btRefresh: Button

    @Inject lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (context?.applicationContext as CustomApplication).appComponent.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tvTitle = view.findViewById(R.id.tv_title)
        tvLastUpdatedTime = view.findViewById(R.id.tv_last_updated_time)
        tvDirection = view.findViewById(R.id.tv_direction)
        rvTrams = view.findViewById(R.id.rv_trams)
        pbProgress = view.findViewById(R.id.pb_progress)
        btRefresh = view.findViewById(R.id.bt_refresh)

        rvTrams.layoutManager = LinearLayoutManager(context)
        rvTrams.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))

        btRefresh.setOnClickListener {
            viewModel.refresh()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        with(viewModel) {
            title.observe(viewLifecycleOwner, {
                tvTitle.text = it
            })

            lastUpdate.observe(viewLifecycleOwner, {
                tvLastUpdatedTime.text = getString(R.string.last_update, it)
            })

            direction.observe(viewLifecycleOwner, {
                tvDirection.text = it
            })

            tramList.observe(viewLifecycleOwner, {
                rvTrams.adapter = Adapter(it)
            })

            progress.observe(viewLifecycleOwner, {
                changeComponentsVisibility(!it)
                pbProgress.isVisible = it
            })

            errorMessage.observe(viewLifecycleOwner, {
                Toast.makeText(context, R.string.error_message, Toast.LENGTH_LONG).show()
            })
        }

        btRefresh.callOnClick()
    }

    private fun changeComponentsVisibility(visibility: Boolean) {
        tvTitle.isVisible = visibility
        tvLastUpdatedTime.isVisible = visibility
        tvDirection.isVisible = visibility
        rvTrams.isVisible = visibility
    }

    private class Adapter(val tramList: List<Tram>): RecyclerView.Adapter<RecyclerView.ViewHolder>(){
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            TextView(parent.context).run {
                setTextColor(ContextCompat.getColor(context, R.color.black))
                return VHItem(this)
            }
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            val tram: Tram = tramList[position]
            (holder as VHItem).view.text = tram.toString()
        }

        override fun getItemCount(): Int {
            return tramList.size
        }
    }

    private class VHItem(itemView: TextView) : RecyclerView.ViewHolder(itemView) {
        var view = itemView
    }
}