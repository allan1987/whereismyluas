package com.whereismyluas.network

import com.whereismyluas.ui.main.model.StopInfo
import retrofit2.http.GET
import retrofit2.http.Query

interface StopInfoApi {

    @GET("get.ashx")
    suspend fun getStopInfo(
        @Query("action") action: String,
        @Query("stop") stop: String,
        @Query("encrypt") encrypt: Boolean
    ): StopInfo
}