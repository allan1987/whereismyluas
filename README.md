# Where is My LUAS ? #

This app uses thee LUAS Forecasting API and provides trams forecast from Marlborough LUAS stop towards Outbound if the user accesses the app from 00:00 to 12:00 and trams forecast from Stillorgan LUAS stop towards Inbound if the user requests the service from 12:01 to 23:59.

### General Description ###

- The project used an Activity and a Fragment for building the single available screen.

- Layouts were built using xml files.

- The Dagger library was used for dependency injection of the components (Activity, ViewModel, Repository, StopInfoApi...), creating then a better resources management.

- As architecture, the MVVM (Model, View, ViewModel) was implemented.

- For server requests, the Retrofit2 library was chosen.

- Also the SimpleXML converter for Retrofit2 was used even though it is deprecated. The usage of JAXB converter is encouraged according to the Java Doc but the respective JAXB github page informs: "Note that JAXB does not work on Android". Some workarounds might be done but it would not aggregate much to the project.

- Initially the Fragment calls the refresh() method from ViewModel, which uses Coroutines (viewModelScope) for asynchronous purposes. So, the getStopInfo() method from the Repository is called. Each LiveData attribute gets its own value updated and also the layout components.

- The error management checks the result of the server call and, in case of any errors, a message is displayed on the screen (Toast message).

- The layout decision was simple, basically a RecyclerView which adapter receives the list of available trams, displaying the destination and due minutes.